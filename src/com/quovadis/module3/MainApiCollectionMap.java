package com.quovadis.module3;

import java.util.*;

public class MainApiCollectionMap {
    public static void main (String[] args){
        Person p1 = new Person("Alice", 23);
        Person p2 = new Person("Brian", 56);
        Person p3 = new Person("Chelsea", 46);
        Person p4 = new Person("David", 28);
        Person p5 = new Person("Erica", 37);
        Person p6 = new Person("Francisco", 18);

        City newYork = new City("New York");
        City shangai = new City("Shangai");
        City mendoza = new City("Mendoza");

        Map<City, List<Person>> map = new HashMap<>();

        map.putIfAbsent(mendoza, new ArrayList<>());
        map.get(mendoza).add(p6);

        map.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p1);
        map.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p2);

        System.out.println("People from Mendoza: " + map.getOrDefault(mendoza, Collections.EMPTY_LIST));
        System.out.println("People from New York: " + map.getOrDefault(newYork, Collections.EMPTY_LIST));

        Map<City, List<Person>> map1 = new HashMap<>();

        map1.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p1);
        map1.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p2);
        map1.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p3);

        System.out.println("Map 1");
        map1.forEach(((city, people) -> System.out.println(city + ":" + people)));

        Map<City, List<Person>> map2 = new HashMap<>();

        map2.computeIfAbsent(shangai, city -> new ArrayList<>()).add(p4);
        map2.computeIfAbsent(mendoza, city -> new ArrayList<>()).add(p5);
        map2.computeIfAbsent(mendoza, city -> new ArrayList<>()).add(p6);

        System.out.println("Map 2");
        map2.forEach(((city, people) -> System.out.println(city + ":" + people)));

        map2.forEach(
                ((city, people) -> {
                    map1.merge(
                            city, people,
                            ((peopleFromMap1, peopleFromMap2) -> {
                                peopleFromMap1.addAll(peopleFromMap2);
                                return peopleFromMap1;
                            })
                    );
                })
        );

        System.out.println("Map 1 merged");
        map1.forEach(((city, people) -> System.out.println(city + ":" + people)));
    }
}
