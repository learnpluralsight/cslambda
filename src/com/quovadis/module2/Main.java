package com.quovadis.module2;

public class Main {
    public static void main(String[] args) {
        // write your code here
        Predicate<String> predicate1 = p -> p.length() < 20;
        Predicate<String> predicate2 = p -> p.length() > 5;

        boolean result = predicate1.test("Hello World");

        System.out.println(result);

        Predicate<String> predicate3 = predicate1.and(predicate2);
        System.out.println("Predicate3 for Yes: " + predicate3.test("Yes"));
        System.out.println("Predicate3 for Good morning: " + predicate3.test("Good morning"));
        System.out.println("Predicate3 for Good morning gentleman: " + predicate3.test("Good morning gentleman"));

        Predicate<String> predicate4 = predicate1.or(predicate2);
        System.out.println("Predicate4 for Yes: " + predicate4.test("Yes"));
        System.out.println("Predicate4 for Good morning: " + predicate4.test("Good morning"));
        System.out.println("Predicate4 for Good morning gentleman: " + predicate4.test("Good morning gentleman"));

        Predicate<String> predicate5 = Predicate.isEqualsTo("Yes");
        System.out.println("Predicate5 for Yes: " + predicate5.test("Yes"));
        System.out.println("Predicate5 for No: " + predicate5.test("No"));

        Predicate<Integer> predicate6 = Predicate.isEqualsTo(5);
        //System.out.println("Predicate6 for Yes: " + predicate5.test(5));
        //System.out.println("Predicate6 for Yes: " + predicate5.test(6));
    }
}
