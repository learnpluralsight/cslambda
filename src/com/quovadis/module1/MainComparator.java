package com.quovadis.module1;

import java.util.function.Function;

public class MainComparator {

    public static void main (String... args){
        Comparator<Person> personAgeComparator = (p1, p2)-> p2.getAge() - p1.getAge();
        Comparator<Person> personFirstNameComparator = (p1, p2)-> p1.getFirstName().compareTo(p2.getFirstName());
        Comparator<Person> personLastNameComparator = (p1, p2)-> p1.getLastName().compareTo(p2.getLastName());

        Function<Person, Integer> personAgeFunction = person -> person.getAge();
        Function<Person, String> personFirstNameFunction = person -> person.getFirstName();
        Function<Person, String> personLastNameFunction = person -> person.getLastName();

        Comparator<Person> personAgeComparatorL = Comparator.comparing(Person::getAge);
        Comparator<Person> personFirstNameComparatorL = Comparator.comparing(Person::getFirstName);
        Comparator<Person> personLastNameComparatorL = Comparator.comparing(Person::getLastName);


        Comparator<Person> personComparator = personAgeComparatorL.thenComparing(personFirstNameComparatorL);

        Comparator<Person> personComparator1 = Comparator.comparing(Person::getLastName)
                                                            .thenComparing(Person::getFirstName)
                                                            .thenComparing(Person::getAge);

    }
}
