package com.quovadis.module4;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class MainReduction {
    public static void main(String [] args){
        List<Integer> ints = Arrays.asList(0,1,2,3,4,5,6,7,8,9);

        List<Integer> ints1 = Arrays.asList(0,1,2,3,4);
        List<Integer> ints2 = Arrays.asList(5,6,7,8,9);

        BinaryOperator<Integer> operator = (i1, i2) -> i1 + i2;
        BinaryOperator<Integer> operatorSimulateParallel = (i1, i2) -> Integer.max(i1, i2);

        int reduction1 = reduce(ints1, 0 , operatorSimulateParallel);
        int reduction2 = reduce(ints1, 0 , operatorSimulateParallel);
        int reductionParalel = reduce(Arrays.asList(reduction1, reduction2), 0 , operatorSimulateParallel);
        System.out.println("Reduction simulate paralel: " + reductionParalel);

        int reduction = reduce(ints, 0 , operator);
        System.out.println("Reduction: " + reduction);


    }

    private static int reduce(List<Integer> values, int valueIfEmpty, BinaryOperator<Integer> reduction) {
        int result = valueIfEmpty;
        for (int value : values){
            result = reduction.apply(result,value);
        }
        return  result;
    }
}
